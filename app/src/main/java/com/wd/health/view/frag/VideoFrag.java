package com.wd.health.view.frag;

import com.wd.base_core.base.BaseFragment;
import com.wd.base_core.mvp.BasePresenter;
import com.wd.health.R;

/*
 *@Auther:陈浩
 *@Date: 2019/8/3
 *@Time:15:55
 *@Description:${DESCRIPTION}
 * */public class VideoFrag extends BaseFragment {
    @Override
    protected void iniData() {

    }

    @Override
    protected int bindlayout() {
        return R.layout.videofrag;
    }

    @Override
    public BasePresenter initPresenter() {
        return null;
    }
}
